# tomsearch

This was originally a masters project for a course on artifial intelligence.
I'm pretty happy with how the code worked out. The goal of the program is to
use astar search to calculate the optimal path through an orinteering course.

In the resources directory is a map of the park where the orienteering course
would occur, and the brown, red, and white text files contain coordinate pairs
for the checkpoints on different courses.

Terrain types are defined by the color of pixels in the terrain map. They are
defined as:

    TERRAIN_OPEN_LAND             0xF89412
    TERRAIN_ROUGH_MEADOW          0xFFC000
    TERRAIN_EASY_MOVEMENT_FOREST  0xFFFFFF
    TERRAIN_SLOW_RUN_FOREST       0x02D03C
    TERRAIN_WALK_FOREST           0x028828
    TERRAIN_IMPASSIBLE_VEGETATION 0x054918
    TERRAIN_LAKE_SWAMP_MARSH      0x0000FF
    TERRAIN_PAVED_ROAD            0x473303
    TERRAIN_FOOTPATH              0x000000
    TERRAIN_OUT_OF_BOUNDS         0xCD0065
    TERRAIN_SAFE_ICE              0x7BFFFF
    TERRAIN_FLOODED_GROUND        0x0094FF

Each terrain type has a somewhat arbitrary difficulty estimated as the relative
difficulty to move over that terrain compared to flat paved ground. Elevation data
from mpp.txt is used to estimate difficulty of moving up and downhill in different
terrain.

The course varies based on which season the competiton takes place in. Summer is
the baseline defined by the terrain map png. In winter water freezes, and ice near 
the shore is safe to walk on. In the fall paths adjacent to easy movement forest 
are more difficult to traverse due to large quantities of fallen leaves.

The changes for orienteering in the spring were the most interesting to implement.
Any pixels within fifteen pixels of water that can be reached from a water pixel 
without gaining more than one meter of elevation (total) are now flooded. These
were found using the generic bfs algorithm in TomSearch.h with a successor
function that takes elevations into account.

This was all pretty cool to implement, and I like how well the templatized search
algorithms lent themselves to reuse throughout the project. For more specifics
take a look at the headers, everything is meticulously commented in doxygen style.

