// File: TerrainMap.cpp
// Author: Thomas Bottom <tdb4197@gmail.com>
//
// See header for doxygen comments

#include <cmath> // for pow & sqrt

// std
#include <iostream> // for cout/cerr
#include <cfloat>   // for FLT_MAX
#include <cstdio>   // for fprintf
#include <functional>  // for function

// Qt5
#include <QFile>

// project
#include "TerrainMap.h"
#include "TomSearch.h"

using namespace std;

// macro to covert an x,y 2D coordinate to a 1D array index
#define MAP_INDEX(x,y) (((x)+((y)*m_width)))
// macro to determine if the given 1D array index is within the valid range
#define IDX_VALID(i) (((i) > -1 && (i) < (m_width*m_height)))

// Initialize constants
const float TerrainMap::k_xscale = 10.29;
const float TerrainMap::k_yscale = 7.55;

#define TERRAIN_OPEN_LAND             0xF89412
#define TERRAIN_ROUGH_MEADOW          0xFFC000
#define TERRAIN_EASY_MOVEMENT_FOREST  0xFFFFFF
#define TERRAIN_SLOW_RUN_FOREST       0x02D03C
#define TERRAIN_WALK_FOREST           0x028828
#define TERRAIN_IMPASSIBLE_VEGETATION 0x054918
#define TERRAIN_LAKE_SWAMP_MARSH      0x0000FF
#define TERRAIN_PAVED_ROAD            0x473303
#define TERRAIN_FOOTPATH              0x000000
#define TERRAIN_OUT_OF_BOUNDS         0xCD0065
#define TERRAIN_SAFE_ICE              0x7BFFFF
#define TERRAIN_FLOODED_GROUND        0x0094FF

#define PATH_DRAW_COLOR               0xFFFF0000 // 100% RED for good visibility. The red paths go faster!

#define PI 3.14159265

// missing QPoint qHash function needed to store a QPoint in a QHash.
// Only worry about 16 bits because we never deal with maps that large anyway.
inline uint qHash (const QPoint & key) {
    return key.x() << 16 | key.y();
}

// Initialize the QMap we use for terrain difficulty lookups with a c++11 initializer list
// IMPORTANT: Difficulty modifiers can ONLY INCREASE the difficulty of a path in order for
// the heuristic straight line distance to be consistent. I.e. multipliers must be >= 1.0
QMap<uint32_t, float> TerrainMap::m_diffMap = {
    {TERRAIN_PAVED_ROAD,            1.0}, // paved road is the baseline 1.0 easiness level
    {TERRAIN_FOOTPATH,              1.1},
    {TERRAIN_OPEN_LAND,             1.2},
    {TERRAIN_EASY_MOVEMENT_FOREST,  1.3},
    {TERRAIN_SLOW_RUN_FOREST,       1.4},
    {TERRAIN_ROUGH_MEADOW,          1.5},
    {TERRAIN_WALK_FOREST,           1.6},
    {TERRAIN_LAKE_SWAMP_MARSH,      FLT_MAX}, // consdier water impassable
    {TERRAIN_IMPASSIBLE_VEGETATION, FLT_MAX}, // consider impassable vegetation impassable
    {TERRAIN_OUT_OF_BOUNDS,         FLT_MAX}, // OOB is defined as maximum difficulty
    {TERRAIN_SAFE_ICE,              2.0},
    {TERRAIN_FLOODED_GROUND,        2.5} // moving through as much as a meter of water is hard
};

// Setup is done in the image load method
TerrainMap::TerrainMap(void)
    : m_locationMap(nullptr),
      m_width(0),
      m_height(0)
{}

TerrainMap::~TerrainMap(void) {
    if(m_locationMap) {
        delete[] m_locationMap;
        m_locationMap = nullptr;
    }
}

TerrainMap::Season TerrainMap::seasonFromString(const QString &str){
    QString lower = str.toLower();
    if("summer" == lower) return e_Summer;
    if("winter" == lower) return e_Winter;
    if("spring" == lower) return e_Spring;
    if("fall"   == lower) return e_Fall;
    return e_Invalid;
}

bool TerrainMap::load(const QString &mapFile, const QString &elevationFile, Season s){
    // Loading an image can fail, so we don't do it in the constructor.
    if(!m_image.load(mapFile)){
        cerr << "Unable to open image file " << mapFile.toStdString() << endl;
        return false;
    }

    // Open the elevation data file
    QFile elevations(elevationFile);
    if(!elevations.open(QIODevice::ReadOnly)){
        cerr << "Unable to open elevations file " << elevationFile.toStdString() << endl;
        return false;
    }

    // The pixel map is considered to be the one and true source for knoweldge about the
    // size of the world. The elevation text file must minimally provide this amount of
    // data (but it can provide more, which will be ignored).
    m_width = m_image.width();   // x axis
    m_height = m_image.height(); // y axis

    // Allocate memory for the location map
    m_locationMap = new Location[m_width * m_height];
    if(nullptr == m_locationMap) {
        cerr << "Unable to allocate memory for location map" << endl;
        return false;
    }

    // From here on out we keep track of whether or not anything has failed so that we
    // can clean up our allocated memory at the end in case of error.
    bool result = true;

    // Initialize this memory to zero. Makes debugging easier.
    memset(m_locationMap, 0, m_width*m_height*sizeof(Location));

    // Attempt to build the location map
    // Loop over all rows
    for(int y = 0; y < m_height && true == result; ++y) {
        QString line = elevations.readLine();
        if(0 < line.size()) {
            // Read in the next line of elevation data
            QStringList tokens = line.split(' ', QString::SkipEmptyParts);

            // Loop over the tokens in this line of data
            int x = 0;
            while(x < m_width && !tokens.empty()) {
                QString nextToken = tokens.takeFirst();
                float z = nextToken.toFloat();

                // Initialize the location struction in our map at coordinate x,y
                int idx = MAP_INDEX(x,y);
                m_locationMap[idx].x = x;
                m_locationMap[idx].y = y;
                m_locationMap[idx].z = z;
                uint32_t rgb = 0xFFFFFF & m_image.pixel(x,y);
                auto diffIt = m_diffMap.find(rgb);

                // Make sure that the pixel maps to a known terrain type
                if(m_diffMap.end() == diffIt) {
                    fprintf(stderr, "Error: Invalid Pixel (%d,%d) in map data. "
                                    "Color = 0x%06X. Unknown terrain type.\n", x, y, rgb);
                    result = false;
                    break;
                }

                // Store the difficulty in the Location so we only ever have to do this
                // map lookup once.
                m_locationMap[idx].d = *diffIt;

                // next column
                ++x;
            }

            // If we haven't already hit an error state AND x is too small
            if(result && x != m_width) { // ran out of columns of data before we were done
                cerr << "Error: Input elevation data has too few columns for map of size "
                     << m_width << " " << m_height << endl;
                result = false;
                break;
            }

        } else { // ran out of rows of data before we were done
            cerr << "Error: Input elevation data has too few rows for map of size "
                << m_width << " " << m_height << endl;
            result = false;
            break;
        }
    }

    // If we failed, clean up
    if(false == result && m_locationMap){
        delete[] m_locationMap;
        m_locationMap = nullptr;
    }

    // Apply any seasonal modifications
    // We can't do this before creating the Location map because we need to know elevations
    setSeason(s);

    // Close the elevations file.
    elevations.close();

    return result;
}

const Location* TerrainMap::getLocation(int x, int y) {
    if(x < m_width && y < m_height) {
        return &m_locationMap[MAP_INDEX(x,y)];
    }else{
        return nullptr;
    }
}

QLinkedList<const Location*> TerrainMap::getSuccessors(const Location *loc) {
    QLinkedList<const Location*> successors;
    int directions[] = {
        MAP_INDEX(loc->x-1, loc->y-1),
        MAP_INDEX(loc->x,   loc->y-1),
        MAP_INDEX(loc->x+1, loc->y-1),
        MAP_INDEX(loc->x-1, loc->y  ),
        MAP_INDEX(loc->x+1, loc->y  ),
        MAP_INDEX(loc->x-1, loc->y+1),
        MAP_INDEX(loc->x,   loc->y+1),
        MAP_INDEX(loc->x+1, loc->y+1)
    };

    for(int i = 0; i < 8; ++i) {
        int &idx = directions[i];
        if(IDX_VALID(idx)) {
            const Location *s = &m_locationMap[idx];
            if(s->d < FLT_MAX) successors.append(s);
        }
    }
    
    return successors;
}

QLinkedList<QPoint> TerrainMap::getAdjacents(const QPoint &p, bool diagonals) {
    QLinkedList<QPoint> successors;
    int x = p.x();
    int y = p.y();
    
    if(x-1 > -1) {
        successors.append(QPoint(x-1, y));
        if(diagonals && y-1 > -1) successors.append(QPoint(x-1, y-1));
        if(diagonals && y+1 < m_height) successors.append(QPoint(x-1, y+1));
    }
    if(x+1 < m_width) {
        successors.append(QPoint(x+1, y));
        if(diagonals && y-1 > -1) successors.append(QPoint(x+1, y-1));
        if(diagonals && y+1 < m_height) successors.append(QPoint(x+1, y+1));
    }
    if(y-1 > -1) successors.append(QPoint(x, y-1));
    if(y+1 < m_height) successors.append(QPoint(x, y+1));

    return successors;
}

void TerrainMap::setSeason(Season s) {
    switch(s) {
        case e_Winter: 
        { 
            // Apply winterization changes. All water more than 7 pixels from land is unsafe to walk on
            // Find all shorelines (water pixels bordering non-water pixels)
            QLinkedList<QPoint> shoreline = getBorders(TERRAIN_LAKE_SWAMP_MARSH);

            /* Set up lambda functions to pass to our generic breadth first search */
            // Goal test fct for our depth-limited winter breadth-first search (is water?)
            std::function<bool(QPoint)> goalTest = [this](QPoint point) {
                if(TERRAIN_LAKE_SWAMP_MARSH == (0x00FFFFFF & m_image.pixel(point.x(),point.y()))) return true;
                else return false;
            }; // goalTest

            // Successor function for qimages. Use capture lambda to give access to m_image.
            std::function<QLinkedList<QPoint>(QPoint)> succFunc = [this](QPoint point) {
                return getAdjacents(point);
            }; // succFunc

            // Perform a depth-limited breadth-first search on each shoreline pixel, discovering all water pixels
            // within 7 pixels of the shoreline. 
            while(!shoreline.empty()) {
                QPoint p = shoreline.takeFirst();
                // the search depth is 6 because we start at water pixels bordering land, at depth 0, and feeze
                // six pixels out from there. This results in at most 7 frozen pixels linearly adjacent to land
                QLinkedList<QPoint> matches = TomSearch::bfs(succFunc, goalTest, p, 6, false);
                 while(!matches.empty()) {
                    QPoint match = matches.takeFirst();
                    m_image.setPixel(match, 0xFF000000 | TERRAIN_SAFE_ICE);
                    // Update the difficulty for this location to that of safe ice
                    const static float iceDifficulty = m_diffMap[TERRAIN_SAFE_ICE];
                    m_locationMap[MAP_INDEX(match.x(), match.y())].d = iceDifficulty;
                 }
            }

            break;
        }
        case e_Summer:
            // Summer is the season of unmodified difficulty.
            break;
        case e_Fall: 
        {
            // "increase the time for any paths through (that is, adjacent to) easy movement forest 
            // (but only those paths)"
            // This is very similar to what getBorders() does, but simply calling that and then processing
            // the resulting QPoints would be inefficient, so the code below is conscientiously very similar
            for(int x = 0; x < m_image.width(); ++x) {
                for(int y = 0; y < m_image.height(); ++y) {
                    uint32_t pixel = 0x00FFFFFF & m_image.pixel(x,y); // ignore alpha channel
                    if(pixel != TERRAIN_FOOTPATH) continue; // We care only about footpaths.
                    // This is a pixel-of-interest, check for adjacency to easy movement forest
                    if((x-1 > -1       && (0x00FFFFFF & m_image.pixel(x-1, y)) == TERRAIN_EASY_MOVEMENT_FOREST)||
                       (x+1 < m_width  && (0x00FFFFFF & m_image.pixel(x+1, y)) == TERRAIN_EASY_MOVEMENT_FOREST)||
                       (y-1 > -1       && (0x00FFFFFF & m_image.pixel(x, y-1)) == TERRAIN_EASY_MOVEMENT_FOREST)||
                       (y+1 < m_height && (0x00FFFFFF & m_image.pixel(x, y+1)) == TERRAIN_EASY_MOVEMENT_FOREST)){
                        // Increase difficulty of moving through this path pixel by 10%
                        m_locationMap[MAP_INDEX(x,y)].d *= 1.1;
                    }
                }
            }
            break;
        }
        case e_Spring: 
        {
            /* Spring: aka "mud season". Any pixels within fifteen pixels of water that can be reached from a 
             * water pixel without gaining more than one meter of elevation (total) are now underwater. 
             * You may choose whether you wish to run through this water or not :) 
             */

            // Goal test lambda fct for our depth-limited spring breadth-first search
            // The successor function is going to handle excluding locations which cannot be reached without
            // gaining more than one meter of elevation, and the bfs implementation handles the 15 pixel depth
            // limitation, so all we need to do here is identify locations which are not already water and are
            // not out of bounds.
            std::function<bool(QPoint)> goalTest = [this](QPoint point) {
                uint32_t terrain = 0x00FFFFFF & m_image.pixel(point.x(),point.y());
                if(terrain != TERRAIN_OUT_OF_BOUNDS &&
                   terrain != TERRAIN_LAKE_SWAMP_MARSH) {
                    return true;
                }
                else return false;
            }; // goalTest

            // The successor function varies per water pixel (more precisely per each water pixels elevation)
            // loop on the water pixels so the lambda can capture that value
            // as in winter, find all shorelines (water pixels bordering non-water pixels)
            QLinkedList<QPoint> shoreline = getBorders(TERRAIN_LAKE_SWAMP_MARSH);
            while(!shoreline.empty()) {
                QPoint p = shoreline.takeFirst();
                
                const Location* location = getLocation(p.x(), p.y());
                const int zmax = location->z + 1;

                // This successor function strips any successor whose elevation more than 1 meter higher than
                // that of the relevant water pixel. It also removes water pixels because we don't need to be
                // searching into the middle of a lake to see if it is under water. Diagonals are not adjacent
                std::function<QLinkedList<QPoint>(QPoint)> succFunc = [this, zmax](QPoint point) {
                    QLinkedList<QPoint> &&adjacents = getAdjacents(point);
                    auto it = adjacents.begin();
                    while(it != adjacents.end()) {
                        const int x = it->x();
                        const int y = it->y();
                        // no water pixels
                        uint32_t pixel = 0xFFFFFF & m_image.pixel(x,y); // ignore alpha
                        if(TERRAIN_LAKE_SWAMP_MARSH == pixel) {
                            it = adjacents.erase(it); // erase returns iterator to next
                            continue;
                        }
                        // no pixels above zmax
                        const Location* l = getLocation(x, y);
                        if(l->z > zmax) {
                            it = adjacents.erase(it); // erase returns iterator to next
                            continue;
                        }
                        // successor found, advance iterator
                        ++it;
                    }

                    return adjacents;
                }; // succFunc

                // Find all pixels within 15 pixels of water 
                QLinkedList<QPoint> matches = TomSearch::bfs(succFunc, goalTest, p, 15, false);
                 while(!matches.empty()) {
                    QPoint match = matches.takeFirst();
                    m_image.setPixel(match, 0xFF000000 | TERRAIN_FLOODED_GROUND);
                    // Update the difficulty for this location to that of flooded ground (passable but slow)
                    const static float floodedDifficulty = m_diffMap[TERRAIN_FLOODED_GROUND];
                    m_locationMap[MAP_INDEX(match.x(), match.y())].d = floodedDifficulty;
                 }
            }

            break;
        }
        default: // Unknown season, print error message and return
            fprintf(stderr, "Unknown season %d in %s, doing nothing\n", s, __func__);
            break;
    }
}

float TerrainMap::straightLineDist(const Location* a, const Location* b) const {
    // 3D Distance d = sqrt((x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2)
    // x and y are pixel locations
    return sqrt( pow( (a->x * k_xscale) - (b->x * k_xscale), 2)
               + pow( (a->y * k_yscale) - (b->y * k_yscale), 2)
               + pow( a->z - b->z, 2) );
    return 0;
}

float TerrainMap::moveCost(const Location* a, const Location* b) const {
    float adjacent = straightLineDist(a,b);
    float cost = adjacent * b->d; // cost is distance times difficulty
    float opposite = b->z - a->z;

    // if b is at a higher elevation than a
    if(opposite > 0) {
        // We know we have a positive angle here.
        float angle = atan(opposite / adjacent);
        // If the angle is more than 45 degrees, we treat is as being 10 times harder because you will either
        // need to spend a lot of time carefully ascending or using special climbing gear
        if(angle > (PI/4)) {
            cost *= 10; // get your climbing kit, this is a steep grade
        } else {
            // The difficulty modifier is set to scale linearly, with it being twice as hard
            // to ascend a 45 degree incline as it is to mvoe across level ground.
            // 1.0 < modifier <= 2.0
            float mult = 2 - (((PI / 4) - angle) / (PI/4));
            cost *= mult;
        }
    }

    // If a is at a higher elevation than b the cost is the same as if they are at the same elevation. Moving
    // downhill requires less energy, but the assumption is that you are moving over level ground at the maximum
    // speed it is safe to do so (e.g. walk forest). You could sprint down a dense forest hill, but you will
    // probably break your ankle. This is in stark contrast to trying to move at your maximum speed for level
    // ground while climbing a steep grade.

    return cost;
}

QLinkedList<const Location*> TerrainMap::getPath(const Location* start, const Location* end) {
    // Set up functions to pass to A*
    // We could std::bind these but the syntax is considerably more dense and the compuiler probably optimizes
    // the lambda to be of equivalent performance. 
    std::function<QLinkedList<const Location*>(const Location*)> successors = [this](const Location* a){
        return getSuccessors(a);
    };
    std::function<float(const Location*, const Location*)> gdist = [this](const Location* a, const Location* b){
        return moveCost(a, b);
    };
    std::function<float(const Location*)> hdist = [this, end](const Location* a){
        return straightLineDist(a,end);
    };

    return TomSearch::astar(successors, gdist, hdist, start);
}

bool TerrainMap::planRoute(const QString &routeFilename, const QString &outFilename) {
    // Open the file with our route coordinates in it
    QFile routeFile(routeFilename);
    if(!routeFile.open(QIODevice::ReadOnly)) {
        cerr << "Error: Unable to open routeFile " << routeFilename.toStdString() << endl;
        return false;
    }

    // Read in each coordinate pair. They are expected as space separated integers, 2 per line
    QLinkedList<const Location*> waypoints;
    while(!routeFile.atEnd()) {
        // Read line and split into two tokens at the space
        QString line = routeFile.readLine();
        QStringList tokens = line.split(' ', QString::SkipEmptyParts);
        if(2 != tokens.size()) {
            cerr << "Error: Invalid format in routeFile " << routeFilename.toStdString()
                 << " cannot parse line:\n" << line.toStdString() << endl;
            return false;
        }

        // Convert tokens to ints. Bind rvalue references to avoid copying QStrings.
        QString &&xs = tokens.takeFirst();
        QString &&ys = tokens.takeFirst();
        int x = xs.toInt();
        int y = ys.toInt();

        // Get and error check the specified Location
        const Location* waypoint = getLocation(x,y);
        if(nullptr == waypoint) {
            cerr << "Error: Waypoint " << x << "," << y << " is out of bounds." << endl;
            return false;
        }
        // waypoint is good, add it to our route
        waypoints.append(waypoint);
    }

    // Get the route!
    const Location* atWp   = nullptr;
    const Location* nextWp = waypoints.takeFirst();
    float travelled = 0; // track total distance
    while(!waypoints.empty()) {
        // Advance!
        atWp = nextWp;
        nextWp = waypoints.takeFirst();

        // find path from at to next
        QLinkedList<const Location*> &&path = getPath(atWp, nextWp);
        if(path.empty()) {
            fprintf(stderr, "Error: no route found betweem %d,%d and %d,%d. Aborting.\n",
                    atWp->x, atWp->y, nextWp->x, nextWp->y);
            return false;
        }

        // draw the path from at to next and add up total distance of route
        const Location* last = nullptr;
        while(!path.empty()) {
            const Location* step = path.takeFirst();
            if(last) travelled += straightLineDist(last, step);
            last = step;
            // Qt supports much fancier methods to paint on an image, but for our purposes a single
            // pixel width path provides the clearest indication of the specified route.
            m_image.setPixel(step->x, step->y, PATH_DRAW_COLOR);
        }
    }

    // Save the result to the output image file
    if(!m_image.save(outFilename)) {
        cerr << "Error: Unable to save output to file " << outFilename.toStdString() << endl;
        return false;
    }

    printf("Path through all waypoints found with total distance %.1f m\n", travelled);

    // Success!
    return true;
}

QLinkedList<QPoint> TerrainMap::getBorders(const uint32_t terrainType) {
    QLinkedList<QPoint> retList;

    // Search over all pixels in the map
    for(int x = 0; x < m_image.width(); ++x) {
        for(int y = 0; y < m_image.height(); ++y) {
            uint32_t pixel = 0x00FFFFFF & m_image.pixel(x,y); // ignore alpha channel
            if(pixel != terrainType) continue; // these are not the pixels we are looking for
            // This is a pixel-of-interest, check for adjacency
            if((x-1 > -1       && (0x00FFFFFF & m_image.pixel(x-1, y)) != terrainType) ||
               (x+1 < m_width  && (0x00FFFFFF & m_image.pixel(x+1, y)) != terrainType) ||
               (y-1 > -1       && (0x00FFFFFF & m_image.pixel(x, y-1)) != terrainType) ||
               (y+1 < m_height && (0x00FFFFFF & m_image.pixel(x, y+1)) != terrainType)) {
                // Border pixel found, add to the list
                retList.append(QPoint(x,y));
            }
        }
    }

    return retList;
}
