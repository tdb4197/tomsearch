/**
 * This class provides an abstraction of a terrain map, encapsulating information about
 * terrain type and elevation.
 */

#ifndef __TERRAIN_MAP_H__
#define __TERRAIN_MAP_H__

// Qt5
#include <QString>
#include <QList>
#include <QMap>
#include <QImage>
#include <QLinkedList>

/**
 * Abstraction of a location in the Terrain Map. These will serve as individual states.
 * Each Location struct contains the x and y coordinates it represents so that the
 * TerrainMap can determine all possible successor states based on it.
 */
typedef struct {
    /** x coordinate of this location in the map. Origin 0 is left */
    int x;
    /** y coordinate of this location in the map. Origin 0 is top */
    int y;
    /** z coordinate (elevation) of this location in the map in meters. */
    float z;
    /** abstraction of the difficulty of traveling through the terrain at this location */
    float d;
} Location;

class TerrainMap {
    public:

    enum Season {
        e_Winter,
        e_Summer,
        e_Fall,
        e_Spring,
        e_Invalid
    };

    // A member-declarator can contain a constant-initializer only if it declares a 
    // static member (9.4) of const integral or const enumeration type, see 9.4.2.
    // initialize in .cpp
    /** scale of a cell along x axis in meters */
    static const float k_xscale;
    /** scale of a cell along y axis in meters */
    static const float k_yscale;

    // ctor / dtor
    TerrainMap(void);
    ~TerrainMap(void);
    
    /**
     * Convert the passed in string to a Season enum
     *
     * @param str - the string to convert, one of [summer,winter,spring,fall]. Case insensitive.
     * @return - a Season enum representing the specified season, or e_Invalid if str is invalid.
     */
    static Season seasonFromString(const QString &str);

    /**
     * Initialize this TerrainMap from a map image file and an elevation file.
     * @param mapFile - relative path to the map image file.
     * @param elevationFile - relative path to the elevation data. Data is expected
     *                        in space delimited floating point format.
     * @return - true iff TerrainMap was successfully initialized from the specified
     *           files.
     */
    bool load(const QString &mapFile, const QString &elevationFile, Season s);

    /**
     * Get a const pointer to the Location structure representing coordinates x,y
     * @param x - the coordinate in the x plane
     * @param y - the coordinate in the y plane
     * @return - a pointer to the specified Location struct, or nullptr if (x,y) is out of
     *           bounds
     */
    const Location* getLocation(int x, int y);

    /**
     * Get all possible successor states to loc. Returned locations are stored in a QList
     * because takeFirst is constant time, and append is amortized constant time. We don't
     * care about order, so this is efficient.
     * @param loc - the state to find successors of
     * @return - a QList of const pointers to all legal successors.
     */
    QLinkedList<const Location*> getSuccessors(const Location *loc);

    /**
     * Get all possible successor pixels in the map specified by p
     * This method does NOT take into account if the terrain specified is actually passable,
     * only that it is a valid pixel on the map.
     * @param p - the pixel to find adjacent pixels to
     * @param diagonals - should diagonal pixels be considered adjacent
     * @return - a QLinkedList of all QPoints represeting valid adjacent pixels
     */
    QLinkedList<QPoint> getAdjacents(const QPoint &p, bool diagonals=false);

    /**
     * Calculate the distance between two Locations taking into account differences in the x and y scaling
     * of pixels and the elevation of each location.
     * This funcations as a basic straight-line distance heuristic.
     *
     * @Param a - one of the locations we want to find the distance between
     * @Param b - one of the locations we want to find the distance between
     * @return - the straight line distance between a and b
     */
    float straightLineDist(const Location* a, const Location* b) const;

    /**
     * Get the cost of moving between two adjacent Locations. This function does NOT check that the Locations
     * are actually adjacent. The general use case for this function is to determine the cost to move between
     * a Location and one of its successors, which are guaranteed to be adjacent.
     *
     * @param a - the Location to move from
     * @param b - the Location to move to
     * @return - the cost of moving from a to b
     */
    float moveCost(const Location* a, const Location* b) const;

    /**
     * Get an optimal path from Location start to Location end.
     *
     * @param start - the starting Location
     * @param end - the end location we are trying to reach
     * @return - A list of nodes in the order we traverse them to get from start to end, empty if start == end.
     *           If no path exists from start to end the returned list is empty.
     */
    QLinkedList<const Location*> getPath(const Location* start, const Location* end);

    /**
     * Plan a route with optimal paths between each pair of coordinates in the file specified by routeFile
     * and output the final routemap as outFile.
     *
     * @param routeFile - the name of the file which contains the x,y coordinate pairs the route must follow
     *                    (one x,y pair per line).
     * @param outFile - the name of the image file to write the result to.
     *                  The filename must be in the form of [filename].png for the output type to be correctly
     *                  inferred.
     */
    bool planRoute(const QString &routeFilename, const QString& outFilename);

    // Private helper functions
    private:
    /**
     * Used internally by load to set seasonal modifiers
     * @param s - season to set
     */
    void setSeason(Season s);

    /**
     * Returns a list of all map coordinates whose terrain is the passed terrainType which
     * border another terrain type. This is used, for example, to find all water pixels which border
     * non-water pixels. This function does not consider diagonal pixels to be adjacent.
     *
     * @param terrainType - RGB value of the pixels you want to get the border of
     * @return - a list of all pixels of the given RGB value which border some other type of terrrain
     *           return type is QLinkedList because adding and removing elements to/from the ends of the list
     *           is a constant time operation.
     */
    QLinkedList<QPoint> getBorders(uint32_t terrainType);

    // protected static data members
    protected:
    /** 
     * This maps the color-coded image map to the baseline difficulties for travelling
     * through each of the represented types of terrain. All distances must be >= 1.0 because actual movement
     * cost is the straight line distance multiplied by the difficulty of the terrain. If a difficulty were less
     * than 1 our heuristic would no longer be consistent.
     */
    static QMap<uint32_t, float> m_diffMap;

    // Private data
    private:
    Location * m_locationMap;
    QImage m_image;
    int m_width;  // x axis
    int m_height; // y axis
};
#endif
