/**
 * The TomSearch namespace provides a repository for generic search algoritms implemented by me.
 * In this file you will find various templated search algorithms designed for general use.
 * In most cases the container class of choice is a LinkedList due to guaranteed constant time insertion and
 * access of elements at either end of the list.
 *
 * @author Thomas Bottom
 */

#ifndef __Tom_SeArCh_H__
#define __Tom_SeArCh_H__

#include <QLinkedList>
#include <QImage>
#include <QQueue>
#include <QSet>
#include <QPair>
#include <QMap>
#include <queue> // for std::priority_queue, Qt lacks an equivalent container

namespace TomSearch {

    /**
     * Perform a breadth first search with optional depth limit for state type S
     * @param getSuccessors - a std::function taking the a state S as an argument, and returning a 
     *                        QLinkedList<S> of successors reachable from it
     * @param goalTest - a std::function taking a state S and returning a boolean representing if it is a 
     *                   goal state
     * @param start - the start state
     * @param depthLimit - how deep can the bfs go? default INT_MAX
     * @param halt - should bfs halt and return the first discovered goal state? default false
     * @return - If halt is true, a QLinkedList<S> containing the first state identified by goalTest as a goal 
     *           state, if no such state exists (or exists within depthLimit), an empty list is returned.
     *           If halt is false, a QLinkedList<S> containing every state identified by goalTest as a goal 
     *           state, in the order they were found, up to depthLimit. As above, if no such state exists within
     *           depthLimit, an empty list is returned.
     */
    template <typename S>
    QLinkedList<S> bfs(std::function<QLinkedList<S>(S)> getSuccessors, 
                       std::function<bool(S)> goalTest, 
                       S start, 
                       int depthLimit=INT_MAX, 
                       bool halt=true) {
        QLinkedList<S> retlist;
        QQueue<QPair<int, S> > frontier;
        QSet<S> explored;

        // place start state on queue, depth 0
        frontier.enqueue(QPair<int, S>(0,start));

        // process the frontier
        while(false == frontier.empty()) {
            QPair<int, S> node = frontier.dequeue();

            // If the node is not explored and does not exceed depth-limit
            if(false == explored.contains(node.second)
                     && node.first < depthLimit) {
                // add to the explored set
                explored.insert(node.second);

                // Check all child nodes
                QLinkedList<S> successors = getSuccessors(node.second);
                while(false == successors.empty()) {
                    S child = successors.takeFirst();

                    if(false == explored.contains(child)) {
                        if(goalTest(child)) {
                            retlist.append(child);
                            if(halt) return retlist;
                        }
                        // enqueue child node, depth+1
                        frontier.enqueue(QPair<int, S>(node.first + 1, child));
                    }
                }
            }
        }
        return retlist;
    }

    /**
     * A* graph search. 
     * Given the successor function and heuristic estimate function hdist, find an optimal path from
     * start to the goal state. The goal state, by definition, is the state for which hdist(state) returns 0.
     *
     * @tparam S - the type of the states
     * @tparam D - the type of the distance measure (e.g. float). Must be a numeric type.
     * @param getSuccessors - a std::function taking the a state S as an argument, and returning a 
     *                        QLinkedList<S> of successors reachable from it.
     * @param gdist - a std::function taking a pair of states and returning the numeric cost of
     *                moving from the fist to the second.
     * @param hdist - a std::function taking a state S as an argument and returning a numeric
     *                distance estimate from S to the goal. This heuristic function must be 
     *                consistent (monotonic) e.g. hdist(n) <= gdist(n, n') + hdist(n')
     */
    template <typename S, typename D>
    QLinkedList<S> astar(std::function<QLinkedList<S>(S)> getSuccessors, 
                         std::function<D(S,S)> gdist, 
                         std::function<D(S)> hdist, 
                         S start) {
        // evaluated nodes
        QSet<S> explored;
        // path to return
        QLinkedList<S> path;

        // mapping from S to the ancestor it can most efficiently be reached from
        // the first element of the pair is the numeric cost to reach that node along the best known path
        // the second is the node it was reached from
        QMap<S,QPair<D,S> > bestPaths;

        // Used to keep track of the goal node. If we get to the end and this is still start, the search failed, or
        // we started at the goal, in either case the path to get there is an empty list, so return that.
        S end = start;

        // Define a less than operator for use in our priority queue.
        // This takes the cost to reach the nodes, adds the estimated cost to the goal, and applies the greater
        // than operator to cause std::priority_queue to function like a min heap instead of max heap.
        // This has the effect of the queue to prioritizing nodes with the smallest f(n) = g(n) + h(n)
        auto queueLess = [hdist](const QPair<D,S> &a, const QPair<D,S> &b){
            // Is the cost to get to a + the estimated cost from a to the goal less than
            //    the cost to get to b + the estimated cost from b to the goal?
            return (a.first + hdist(a.second)) > (b.first + hdist(b.second));
        };
        // Priority queue of nodes to evaluate (using the less function we just defined)
        std::priority_queue<QPair<D,S>, std::vector<QPair<D,S> >, decltype(queueLess)> frontier(queueLess);
        frontier.emplace(0,start); // start node, cost 0

        // Commence the search
        while(frontier.size()) {

            // Get lowest estimated distance element off of the queue
            QPair<D,S> current = frontier.top(); 
            frontier.pop();

            // Skip exploring this node if we have already done so
            if(explored.contains(current.second)) continue;

            // Goal Test. Heuristics must not overestimate the distance to the goal,
            // so if this is the goal, the distance must be zero.
            if(hdist(current.second) == 0) {
                end = current.second;
                break;
            }

            // we are evaluating current, add it to the explored set
            explored.insert(current.second);

            // get successors to current
            QLinkedList<S> successors = getSuccessors(current.second);
            while(!successors.empty()) {
                S next = successors.takeFirst();

                // for each successor, if we have not explored it yet
                if(!explored.contains(next)) {
                    // compute cost of reaching next from current
                    D g = current.first + gdist(current.second, next);
                    // add it to the priority queue
                    // Don't worry about duplicates in the queue, redundant higher cost paths to reach a node
                    // will never be evaluated
                    frontier.emplace(g, next);

                    // If we have seen this node before
                    auto it = bestPaths.find(next);
                    if(it != bestPaths.end()) {
                        if(g < it->first) {
                            it->first = g;
                            it->second = current.second;
                        }
                    } else {
                        // new node, so this is the best path to reach it
                        bestPaths.insert(next, QPair<D,S>(g, current.second));
                    }
                }
            }
        }

        // Reconstruct the path (empty if start is the goal or there is no path to the goal)
        while(end != start) {
            path.prepend(end);
            end = bestPaths[end].second;
        }

        return path;
    }

} // namespace
#endif
