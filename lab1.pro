######################################################################
# Automatically generated by qmake (3.0) Sun Feb 25 19:05:31 2018
######################################################################

QT+=gui
INCLUDEPATH+=./include
CONFIG+=c++11
TEMPLATE = app
TARGET = lab1
INCLUDEPATH += .

# Input
HEADERS += include/Location.h include/TerrainMap.h
SOURCES += main.cpp TerrainMap.cpp
