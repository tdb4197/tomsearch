// File: main.cpp
// Author: Thomas Bottom <tdb4197@cs.rit.edu>
//
// main for CSCI-630 lab 1

#include <cstdio>

#include "TerrainMap.h"

// Print usage message and exit
void usage(void){
    printf("Usage: lab1 mapFile elevationFile routeFile outFilename [season]\n"
           "    mapFile       the map image to navigate\n"
           "    elevationFile a text file of map elevation data\n"
           "    routeFile     a text file of coordinate pairs to navigate\n"
           "    outFilename   name of the output map file\n"
           "    [season]      the season to navigate in, defaults to summer\n"
           );
}

// Some handy testing for use during development
void doTests(TerrainMap &map) {
    const Location* interest = map.getLocation(227,452);
    printf("%d,%d has elevation of %f\n", interest->x, interest->y, interest->z);
    
    const Location* p0 = map.getLocation(220,452);
    const Location* p1 = map.getLocation(228,453);
    printf("%d,%d has dist of %f\n", p1->x, p1->y, map.straightLineDist(p1, p0));
    const Location* p2 = map.getLocation(226,453);
    printf("%d,%d has dist of %f\n", p2->x, p2->y, map.straightLineDist(p2, p0));

    // get a test distance
    float dist = map.straightLineDist(map.getLocation(0,0), map.getLocation(1,1));
    float cost = map.moveCost(map.getLocation(0,0), map.getLocation(1,1));
    float iden = map.straightLineDist(map.getLocation(0,0), map.getLocation(0,0));
    printf("Dist = %f\n", dist);
    printf("Cost = %f\n", cost);
    printf("Iden = %f\n", iden);

    // Validate some successors
    QLinkedList<const Location*> succ = map.getSuccessors(map.getLocation(227,452));
    while(!succ.empty()) {
        const Location* foo = succ.takeFirst();
        printf("successor = %d,%d\n", foo->x, foo->y);
    }

    // Get test route
    map.planRoute("resources/brown.txt", "foo.png");
}

int main(int argc, char ** argv){
    // minimal sanity check of inputs
    if(argc < 5 || argc > 6) {
        usage();
        return 1;
    }

    // Parse the season (summer is default if none given)
    TerrainMap::Season season = TerrainMap::e_Summer;
    if(6 == argc) {
        season = TerrainMap::seasonFromString(argv[5]);
    }

    // Build the initial state
    TerrainMap map;
    bool result = map.load(argv[1], argv[2], season);
    if(!result) {
        fprintf(stderr, "Error loading map\n");
        usage();   
        return 0;
    }

    //doTests(map);
    //return 0;

    // Fire It Up!
    map.planRoute(argv[3], argv[4]);

    return 0;
}
